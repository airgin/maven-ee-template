package de.eo.sample.web;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import de.eo.sample.service.Person;
import de.eo.sample.service.PersonService;

@Stateless
@Path("/person")
public class PersonRessource {

	@Inject
	private PersonService personService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Person getPerson() {
		return personService.getPerson();
	}

}
